﻿namespace vizeOrnek
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEkle = new System.Windows.Forms.Button();
            this.txtSayılar = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.sayiListe = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnEkle
            // 
            this.btnEkle.Location = new System.Drawing.Point(227, 33);
            this.btnEkle.Name = "btnEkle";
            this.btnEkle.Size = new System.Drawing.Size(104, 23);
            this.btnEkle.TabIndex = 0;
            this.btnEkle.Text = "ListeyeEkle";
            this.btnEkle.UseVisualStyleBackColor = true;
            this.btnEkle.Click += new System.EventHandler(this.btnEkle_Click);
            // 
            // txtSayılar
            // 
            this.txtSayılar.Location = new System.Drawing.Point(34, 36);
            this.txtSayılar.Name = "txtSayılar";
            this.txtSayılar.Size = new System.Drawing.Size(116, 20);
            this.txtSayılar.TabIndex = 1;
            this.txtSayılar.Text = "Sayılar:3 0 5 6 7 -5";
            this.txtSayılar.TextChanged += new System.EventHandler(this.txtSayılar_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(34, 344);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 27);
            this.button2.TabIndex = 3;
            this.button2.Text = "Hesapla";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // sayiListe
            // 
            this.sayiListe.FormattingEnabled = true;
            this.sayiListe.Location = new System.Drawing.Point(34, 88);
            this.sayiListe.Name = "sayiListe";
            this.sayiListe.Size = new System.Drawing.Size(120, 134);
            this.sayiListe.TabIndex = 4;
            this.sayiListe.SelectedIndexChanged += new System.EventHandler(this.sayiListe_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 450);
            this.Controls.Add(this.sayiListe);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtSayılar);
            this.Controls.Add(this.btnEkle);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEkle;
        private System.Windows.Forms.TextBox txtSayılar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox sayiListe;
    }
}

